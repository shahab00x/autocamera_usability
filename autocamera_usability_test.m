classdef autocamera_usability_test < handle
    properties
        types = struct('cartesian', 'cartesian', 'joint', 'joint');
        data = {};
        data_type = '';
    end
    methods
        function self = autocamera_usability_test()
        end
        
        function r = open_file(self, type)
            f = uigetfile('*txt', 'Select data file');
            r = self.read_data(f, type);
        end
        function r = find_camera_look_at_point(self, psm1, psm2, ecm, keyhole)
            vector1 = psm1-psm2;
            norm = ecm-keyhole;
            vector2 = cross(vector1, norm);
            
            % Find the equation of the line that passes through the keyhole
            % and the ecm end-effector
            x = @(t) keyhole(1) + norm(1) * t ;
            y = @(t) keyhole(2) + norm(2) * t ;
            z = @(t) keyhole(3) + norm(3) * t ;
            
            t = ( norm(1) * psm1(1) + norm(2) * psm1(2) + norm(3) * psm1(3)  )/ ...
                (  norm(1) * (keyhole(1) + norm(1)) +  norm(2) * (keyhole(2) + norm(2)) +  norm(3) * (keyhole(3) + norm(3))  );
            
            
            r = [x(t), y(t), z(t)];
        end
        
        function r = read_data(self, file_name, type)
            f = strsplit(fileread( strcat('data/', file_name)), '\n');
            row_num = 0;
            data = {};
            for i = 1:6:(numel(f)-6)
                data{end+1} = struct('ecm' , str2num(f{i+1}), 'psm1' , str2num(f{ i+3}), 'psm2' , str2num(f{ i+5}));
            end
            self.data = data;
            self.data_type = type;
            r = true;
        end
        
        function r = plot_graph(self, num)
            e = struct('x',[], 'y',[],'z',[]);
            temp_e = struct('x', 0, 'y', 0, 'z', 0);
            p1 = struct('x',[], 'y',[],'z',[]);
            p2 = struct('x',[], 'y',[],'z',[]);
            dist_p1_e = [];
            dist_p2_e = [];
            
            if num == 1
                for i=1:numel(self.data)
                    psm1_point = self.data{i}.psm1(1:3);
                    psm2_point = self.data{i}.psm2(1:3);
                    ecm_point = self.data{i}.ecm(1:3);
                    keyhole = self.data{1}.ecm(1:3);
                    
                    new_e = self.find_camera_look_at_point(psm1_point, psm2_point, ecm_point, keyhole);
                    
                    e.x(end+1) = self.data{i}.ecm(1);
                    e.y(end+1) = self.data{i}.ecm(2);
                    e.z(end+1) = self.data{i}.ecm(3);
                    
                    p1.x(end+1) = self.data{i}.psm1(1);
                    p1.y(end+1) = self.data{i}.psm1(2);
                    p1.z(end+1) = self.data{i}.psm1(3);
                    
                    p2.x(end+1) = self.data{i}.psm2(1);
                    p2.y(end+1) = self.data{i}.psm2(2);
                    p2.z(end+1) = self.data{i}.psm2(3);

                    dist_p1_e(end+1) = pdist( [psm1_point; new_e], 'EUCLIDEAN');
                    dist_p2_e(end+1) = -pdist( [new_e; psm2_point], 'EUCLIDEAN');
                end
                plot(1:numel(dist_p1_e), dist_p1_e, 1:numel(dist_p1_e), dist_p2_e);
%                 subplot(1,3,1);
%                 hold on
%                 scatter3(e.x,e.y,e.z, '*');
% %                 subplot(1,3,2);
%                 hold on
%                 scatter3(p1.x,p1.y,p1.z,'+');
% %                 subplot(1,3,3);
%                 hold on
%                 scatter3(p2.x,p2.y,p2.z,'x');
                
            end
            
            
        end
    end
end